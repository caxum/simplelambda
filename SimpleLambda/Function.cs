using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;


using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace SimpleLambda
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string FunctionHandler(string input, ILambdaContext context)
        {
            var isSuccesssful = false;
            context.Logger.Log("begin");
            
            var InvoiceList = new List<string>();
            try
            {

          
            using (var Conn = new SqlConnection(System.Environment.GetEnvironmentVariable("championProductsDBCOnnString")))
            {

                using (var cmd = new SqlCommand("Select InvoiceId from Invoice", Conn))
                {
                    Conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        InvoiceList.Add(rdr[0].ToString());
                    }
                    Conn.Close();
                   foreach (var item in InvoiceList)
                    {
                        using (var updateCmd = new SqlCommand("Update dbo.Invoice Set PaidDate = GetDate() where InvoiceId = " + item, Conn)) 
                        {
                            Conn.Open();
                            updateCmd.ExecuteNonQuery();
                            Conn.Close();
                        }

                    }
                    Conn.Close();
                 

                }
            }
                isSuccesssful = true;
            }
            catch (Exception)
            {

                isSuccesssful = false;
            }

            return input?.ToUpper();


        }
    }
}
